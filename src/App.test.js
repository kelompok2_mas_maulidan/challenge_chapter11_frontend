import { cleanup, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import App from './App';

afterEach(() => {
  cleanup();
})

describe("App Component", () => {
  test('renders LOGO link', () => {
    render(<App />);
    const logoElement = screen.getByText("LOGO");
    expect(logoElement).toBeInTheDocument();
  });
});

