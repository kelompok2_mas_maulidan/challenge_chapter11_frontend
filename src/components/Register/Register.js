import React, { useState } from "react";
import axios from 'axios';
import qs from 'qs';
import styles from './Register.css';

function Register(){
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
    const [email,setEmail] = useState("");
    const [bio, setBio] = useState("");
    const [city, setCity] = useState("");
    const [socialMedia, setSocialMedia] = useState("");

    let handleSubmit = () => {
        const data = qs.stringify({
            'username': username,
            'password': password,
            'email': email,
            'bio': bio,
            'city': city,
            'social_media_url': socialMedia
        });
        const config = {
            method: 'post',
            url: 'http://localhost:8080/api/register',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            data: data
        };

        if ( username === "" && password && "" && email === "" ) {
           document.querySelector(".error").innerHTML = "Please Fill The Require Field";
        } else {
            document.querySelector(".error").innerHTML = "";

            axios(config)
            .then((res) => {
              
                console.log(JSON.stringify(res.data.data));
                alert(res.data.message);
                window.location="/Login";
            })
            .catch(err => {

                console.log(err.response.data);
                document.querySelector(".error").innerHTML = err.response.data.message
            });
        }
    }
    
    return(
        <>
            <div className="background-register">
            </div>
            <section className="input">
                <div className="inputContainer">
                    <h1>REGISTER</h1>
                    
                    <div className="inputBox">
                        <input type="email" required="required" placeholder="Your Email" name="email" onChange={(value)=>setEmail(value.target.value)}/>
                        <span>Email</span>
                    </div>
                    
                    <div className="inputBox">
                        <input type="text" required="required" placeholder="Your Username" name="username"onChange={(value)=>setUsername(value.target.value)}/>
                        <span>Username</span>
                    </div>
                    
                    <div className="inputBox">
                        <input type="password" required="required" name="password"onChange={(value)=>setPassword(value.target.value)}/>
                        <span>Password</span>
                    </div>
                    
                    <div className="inputBox">
                        <input type="text" name="bio" onChange={(value)=>setBio(value.target.value)}/>
                        <span>Bio</span>
                    </div>

                    <div className="inputBox">
                        <input type="text" name="city" onChange={(value)=>setCity(value.target.value)}/>
                        <span>City</span>
                    </div>

                    <div className="inputBox">
                        <input type="text" name="socialMedia" onChange={(value)=>setSocialMedia(value.target.value)}/>
                        <span>Social Media URL</span>
                    </div>
                    
                    <div className="inputBox">
                        <h5 className="error"></h5>
                        <button className="buttonLogin" onClick={()=>handleSubmit()}>Register</button>
                        <a className="login-nav"href="/">Back</a>
                    </div>
                </div>   
            </section>
        </>
    )
}
export default Register;