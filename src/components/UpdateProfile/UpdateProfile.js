import React, { useEffect, useState } from 'react'
import NavBar from '../Navbar/Navbar';
import "./UpdateProfile.css"
import axios from 'axios';
import { useNavigate } from 'react-router';



function UpdateProfile(){
    const token = sessionStorage.getItem("accessToken");
    const id = sessionStorage.getItem("id");
    const url = "http://localhost:8080/api/user/"+id;

    const [currentUser, setCurrentUser] = useState("");

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [username, setUsername] = useState("");
    const [bio, setBio] = useState("");
    const [city, setCity] = useState("");
    const [social_media_url, setSocialMediaURL] = useState("");

    useEffect(() => {
        axios.get('http://localhost:8080/api/user/'+id, {
            headers: {
                'Authorization': token,
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(res => {
          setCurrentUser(res.data);
          setEmail(res.data.email);
          setPassword(res.data.password);
          setUsername(res.data.username);
          setBio(res.data.bio);
          setCity(res.data.city);
          setSocialMediaURL(res.data.social_media_url);
          
        });
      }, []);

    const navigation = useNavigate();

    let handleSubmit = () => {
        if (email === "" && username === "" && bio === "" && city === "" && social_media_url === "") {
            document.querySelector(".error").innerHTML = "Please Fill the Form";
        } else {
            if (password === '') {
                let password = password;
            }
            axios.put(url, {
                email: email,
                username: username,
                bio: bio,
                city: city,
                social_media_url: social_media_url,
                password: password
            }, {
                headers: {
                    'Authorization': token
                }
            })
            .then(res => {
                alert(res.data.message);
                window.location.replace("/Profile");
            }).catch(err => {
                console.log(err);
            });
        }
    }
    
    function goToProfile() {
        navigation("/Profile");
    }

    return (
    <>
    {/* <NavBar /> */}
    <div className='main'>
    <div className='row container-height' >
        <div className='col-lg-6 col-md-6 m-auto' >
            <div className='container' >
                <h1 className='text-center'>Update Profile</h1>
                    <form >
                        <fieldset>
                            <div className='form-group' >
                                <label htmlFor='exampleInputEmail'>Email</label>
                                <input
                                    type="email"
                                    name='email'
                                    className='form-control'
                                    defaultValue={email || ''}
                                    required="required"
                                    onChange={(value) => setEmail(value.target.value)}
                                />
                            </div>
                            <div className='form-group' >
                                <label htmlFor='exampleInputEmail'>Password</label>
                                <input
                                    type="password"
                                    name='password'
                                    className='form-control'
                                    defaultValue={''}
                                    placeholder="minimum 6 karakter"
                                    onChange={(value)=>setPassword(value.target.value)}
                                />
                            </div>
                            <div className='form-group'>
                                <label htmlFor='exampleInputName' >Username</label>
                                <input
                                    type="text"
                                    name='username'
                                    className='form-control'
                                    defaultValue={username || ''}
                                    required="required"
                                    onChange={(value)=>setUsername(value.target.value)}
                                />
                            </div>
                            <div className='form-group' >
                                <label htmlFor='exampleInputBio'>Bio</label>
                                <input
                                    type="text"
                                    name='bio'
                                    className='form-control'
                                    defaultValue={bio || ''}
                                    required="required"
                                    onChange={(value)=>setBio(value.target.value)}
                                />
                            </div>
                            <div className='form-group' >
                                <label htmlFor='exampleInputCity'>City</label>
                                <input
                                    type="text"
                                    name='city'
                                    className='form-control'
                                    defaultValue={city || ''}
                                    required="required"
                                    onChange={(value)=>setCity(value.target.value)}
                                />
                            </div>
                            <div className='form-group' >
                                <label htmlFor='exampleInputSosial'>Social Media Url</label>
                                <input
                                    type="text"
                                    className='form-control'
                                    name='social_media_url'
                                    defaultValue={social_media_url || ''}
                                    required="required"
                                    onChange={(value)=>setSocialMediaURL(value.target.value)}
                                />
                            </div>
                            <br />
                            <h5 style={{color:"white"}}  className="error"></h5>
                           
                        </fieldset>
                    </form>
                    <button
                            className='btn btn-primary'
                            onClick={()=> handleSubmit()}
                            >
                                Update your profile
                            </button>
                            <button
                            className='btn btn-info' name='back'
                            onClick={()=> goToProfile()}
                            >
                                Back
                            </button>
            </div>
        </div>
    </div>
    </div>
    </>
  )
}


export default UpdateProfile;