import axios from "axios";
import React, { useEffect, useRef, useState } from "react";
import styles from './NavBar.css';

function NavBar(){
  const [currentUser,setUser] = useState({ token: "", username: "" });
  const token = sessionStorage.getItem("accessToken");
  const id = sessionStorage.getItem("id");
  const url = 'http://localhost:8080/api/user/'+id;

  useEffect(() => {

    if (token != null) {
      axios.get(url, {
        headers: {
          'Authorization': token
        }
      }).then(res => {
        setUser({ token: token, username: res.data.username });
      });
    }
    
  }, [])

    return(
        <>
        <nav className="navbar navbar-expand-lg ">
    <a className="navbar-brand" href="#">LOGO</a>
    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarNav">
      <ul className="navbar-nav mr-auto left-navbar">
        <li className="nav-item active">
          <a className="nav-link" href="/">HOME<span className="sr-only">(current)</span></a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="/List">GAME LIST</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">CONTACT</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="#">ABOUT ME</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="/videocloud">Video</a>
        </li>
        <li className="nav-item">
          <a className="nav-link" href="/audiocloud">Audio</a>
        </li>
      </ul>
      <ul className="navbar-nav ml-auto right-navbar">
      
        <li className="nav-item active">
          <a className="nav-link" href={currentUser.token != '' ? "/profile" : "/register" }>{currentUser.token != '' ? currentUser.username.charAt(0).toUpperCase() + currentUser.username.slice(1) : "REGISTER"}<span className="sr-only">(current)</span></a>
        </li>
        <li className="nav-item">
          <a className="nav-link"onClick={currentUser.token != '' ? ()=>/*console.log("LOGOUT")*/{sessionStorage.removeItem("accessToken");window.location="/"}:()=>{console.log("LOGIN"); window.location="/Login"}} href="#">{currentUser.token != '' ? "LOGOUT" : "LOGIN"}</a>
        </li>
      
      </ul>
    </div>
  </nav>
        </>
    )
}
export default NavBar;