import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { HomePage } from '../controller/PageController';
import Profile from '../Components/Profile/Profile';
import UpdateProfile from '../Components/UpdateProfile/UpdateProfile';
import { RegisterPage, LoginPage } from '../controller/Auth';
import GameList from '../Components/Games/GameList';
import DetailGame from '../Components/Games/Details/BatuGuntingKertas/BatuGuntingKertas';
import Video from '../Components/Video/Video';
import Videocloud from '../Components/Videocloud/Videocloud';
import Audio from '../Components/Audio/Audio';
import Audiocloud from '../Components/Audiocloud/Audiocloud';
import Index from '../Components/Games/BatuGuntingKertas/Index';

class Router extends React.Component {
    render() {
        return (
            <BrowserRouter>
                <Routes>
                    <Route path='/' element={<HomePage />} />
                    <Route path='/Login' element={<LoginPage />} />
                    <Route path='/Register' element={<RegisterPage />} />
                    <Route path='/Profile' element={<Profile />} />
                    <Route path='/Update' element={<UpdateProfile />} />
                    <Route path='/List' element={<GameList />} />
                    <Route path='/DetailGames' element={<DetailGame />} />
                    <Route path='/GamesBatuGuntingKertas' element={<Index />} />
                    <Route path='/Video' element={<Video />} />
                    <Route path='/Audio' element={<Audio />} />
                    <Route path='*' element={<h1>NOT FOUND</h1>} />
                </Routes>
            </BrowserRouter>
        );
    }
}
export default Router;