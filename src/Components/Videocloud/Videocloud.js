import React, { useState } from 'react'
import {Player, ControlBar} from 'video-react'
import 'video-react/dist/video-react.css';
const App = () => {
  
const [video, setVideo ] = useState("");
const [ url, setUrl ] = useState("");
const uploadImage = () => {
const data = new FormData()
data.append("file", video)
data.append("upload_preset", "VIDEO_PRESET")
data.append("cloud_name","dam5dp3mq")
fetch("  https://api.cloudinary.com/v1_1/dam5dp3mq/video/upload",{
method:"post",
body: data
})
.then(resp => resp.json())
.then(data => {
setUrl(data.url)
})
.catch(err => console.log(err))
}
return (
<div>
<div>
<input type="file" onChange= {(e)=> setVideo(e.target.files[0])}></input>
<button onClick={uploadImage}>Upload</button>
</div>
<div>
<h1>Uploaded video will be displayed here</h1>
<img src={'https://res.cloudinary.com/dam5dp3mq/video/upload/v1664358435/VIDEO/cxi62gtujp4gtpbos8mo.mp4'}/>
<Player>
        <source src={url}/>
        <ControlBar autoHide={false}/>
</Player>
</div>
</div>
)
}
export default App;