import React, { useState } from "react";
// import { Player, ControlBar } from "video-react";
// import "video-react/dist/video-react.css";
import "./styles.css";
import AudioPlayer from "react-h5-audio-player";
import "react-h5-audio-player/lib/styles.css";
const App = () => {
  const [video, setVideo] = useState("");
  const [url, setUrl] = useState("");
  const uploadImage = () => {
    const data = new FormData();
    data.append("file", video);
    data.append("upload_preset", "AUDIO_PRESET");
    data.append("cloud_name", "dam5dp3mq");
    fetch("  https://api.cloudinary.com/v1_1/dam5dp3mq/video/upload", {
      method: "post",
      body: data,
    })
      .then((resp) => resp.json())
      .then((data) => {
        setUrl(data.url);
      })
      .catch((err) => console.log(err));
  };
  const musicTracks = [
        {
          name: "Music1",
          src: "https://res.cloudinary.com/dam5dp3mq/video/upload/v1664361593/AUDIO/p3yq5iswspjzlhsg0p05.mp3"
        },
        {
          name: "Music2",
          src: "https://res.cloudinary.com/dam5dp3mq/video/upload/v1664361432/AUDIO/phe2qpqalefqmoswvucx.mp3"
        }
      ];
    
      const [trackIndex, setTrackIndex] = useState(0);
    
      const handleClickPrevious = () => {
        setTrackIndex((currentTrack) =>
          currentTrack === 0 ? musicTracks.length - 1 : currentTrack - 1
        );
      };
    
      const handleClickNext = () => {
        setTrackIndex((currentTrack) =>
          currentTrack < musicTracks.length - 1 ? currentTrack + 1 : 0
        );
      };
  return (
    <div>
      <div>
        <input
          type="file"
          onChange={(e) => setVideo(e.target.files[0])}
        ></input>
        <button onClick={uploadImage}>Upload</button>
      </div>
      <div>
        <h1>Uploaded video will be displayed here</h1>
        <img
          src={
            "https://res.cloudinary.com/dam5dp3mq/video/upload/v1664358435/VIDEO/cxi62gtujp4gtpbos8mo.mp4"
          }
        />
        <AudioPlayer
        // style={{ width: "300px" }}
        style={{ borderRadius: "1rem" }}
        autoPlay
        // layout="horizontal"
        src={musicTracks[trackIndex].src}
        onPlay={(e) => console.log("onPlay")}
        showSkipControls={true}
        showJumpControls={false}
        header={`Now playing: ${musicTracks[trackIndex].name}`}
        onClickPrevious={handleClickPrevious}
        onClickNext={handleClickNext}
        onEnded={handleClickNext}
        // other props here
      />
      </div>
    </div>
  );
};
export default App;
