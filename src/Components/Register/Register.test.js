import { cleanup, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import App from './Register';

afterEach(() => {
  cleanup();
})

describe("Register Component", () => {
  test('renders Email input', () => {
    render(<App />);
    const emailElement = screen.getByPlaceholderText("Your Email");
    expect(emailElement).toBeInTheDocument();
  });

  test('renders Username input', () => {
    render(<App />);
    const emailElement = screen.getByPlaceholderText("Your Username");
    expect(emailElement).toBeInTheDocument();
  });
});

