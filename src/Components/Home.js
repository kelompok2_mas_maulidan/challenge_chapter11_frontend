import React, { Component } from 'react';

class Home extends Component {
    user={value:'andri'};
    render() {
        return (
            <body>
                <nav className="navbar navbar-expand-lg navbar-light w-100">
                    <div className="container position-relative">
                        <a className="navbar-brand text-white" href="#">
              LOGO
                        </a>
                        <button
                            className="navbar-toggler"
                            type="button"
                            data-bs-toggle="collapse"
                            data-bs-target="#navbarSupportedContent"
                            aria-controls="navbarSupportedContent"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                        >
                            <i
                                className="fa fa-bars"
                                aria-hidden="true"
                                style="display: block;"
                            ></i>
                            <i
                                className="fa fa-times"
                                aria-hidden="true"
                                style="display: none;"
                            ></i>
                        </button>
                        <div
                            className="collapse navbar-collapse justify-content-between"
                            id="navbarSupportedContent"
                        >
                            <div>
                                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                    <li className="nav-item">
                                        <a className="nav-link text-white" href="#">
                      HOME
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link text-white" href="#">
                      WORK
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link text-white" href="#">
                      CONTACT
                                        </a>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link text-white" href="#">
                      ABOUT ME
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                                    <li className="nav-item">
                           
                                        <span className="nav-link text-white">Hai,{this.state.value} </span>
                                    </li>
                                    <li className="nav-item">
                                        <a className="nav-link text-white" href="/logout">LOGOUT</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>

                <section className="container-fluid main-background">
                    <div className="home-content d-flex flex-column justify-content-center align-items-center position-relative">
                        <div className="text-white home-title mb-5">
                            <span>PLAY TRADITIONAL GAME</span>
                        </div>
                        <div className="text-white home-description mb-5">
                            <span>Experience new traditional game play</span>
                        </div>
                        <div className="home-button">
                            <button onClick="location.href = '/game';">PLAY NOW</button>
                        </div>
                        <div className="home-navigator position-absolute">
                            <a href="#" className="d-flex flex-column align-items-center">
                                <span>THE STORY</span>
                                <img src="../public/images/index/scroll-down.svg" alt="" />
                            </a>
                        </div>
                    </div>
                </section>

                <section className="container-fluid d-flex the-games-background">
                    <div className="the-games-content d-flex align-items-center justify-content-center">
                        <div className="the-games-text d-flex flex-column">
                            <span className="the-games-text-top text-white">
                What's so special?
                            </span>
                            <span className="the-games-text-bottom text-white">THE GAMES</span>
                        </div>
                        <div className="the-games-carousel">
                            <div
                                id="carouselExampleIndicators"
                                className="carousel slide"
                                data-bs-ride="carousel"
                            >
                                <div className="carousel-indicators">
                                    <button
                                        type="button"
                                        data-bs-target="#carouselExampleIndicators"
                                        data-bs-slide-to="0"
                                        className="active"
                                        aria-current="true"
                                        aria-label="Slide 1"
                                    ></button>
                                    <button
                                        type="button"
                                        data-bs-target="#carouselExampleIndicators"
                                        data-bs-slide-to="1"
                                        aria-label="Slide 2"
                                    ></button>
                                    <button
                                        type="button"
                                        data-bs-target="#carouselExampleIndicators"
                                        data-bs-slide-to="2"
                                        aria-label="Slide 3"
                                    ></button>
                                </div>
                                <div className="carousel-inner">
                                    <div className="carousel-item active">
                                        <img
                                            src="images/index/rockpaperstrategy-1600.jpg"
                                            className="d-block w-100"
                                            alt="rockpaperstrategy-1600"
                                        />
                                    </div>
                                    <div className="carousel-item">
                                        <img
                                            src="images/index/rockpaperstrategy-1600.jpg"
                                            className="d-block w-100"
                                            alt="rockpaperstrategy-1600"
                                        />
                                    </div>
                                    <div className="carousel-item">
                                        <img
                                            src="images/index/rockpaperstrategy-1600.jpg"
                                            className="d-block w-100"
                                            alt="rockpaperstrategy-1600"
                                        />
                                    </div>
                                </div>
                                <button
                                    className="carousel-control-prev"
                                    type="button"
                                    data-bs-target="#carouselExampleIndicators"
                                    data-bs-slide="prev"
                                >
                                    <span
                                        className="carousel-control-prev-icon"
                                        aria-hidden="true"
                                    ></span>
                                    <span className="visually-hidden">Previous</span>
                                </button>
                                <button
                                    className="carousel-control-next"
                                    type="button"
                                    data-bs-target="#carouselExampleIndicators"
                                    data-bs-slide="next"
                                >
                                    <span
                                        className="carousel-control-next-icon"
                                        aria-hidden="true"
                                    ></span>
                                    <span className="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="container-fluid d-flex justify-content-end align-items-center features-background">
                    <div className="d-flex flex-column mobile-card-glass">
                        <div className="d-flex flex-column mb-5">
                            <span className="features-subtitle text-white">
                What's so special
                            </span>
                            <span className="features-title text-white">FEATURES</span>
                        </div>
                        <div className="d-flex flex-column features-timeline">
                            <div className="d-flex mb-5">
                                <div className="me-5">
                                    <img src="images/index/dot-white.svg" alt="" />
                                </div>
                                <div className="d-flex flex-column">
                                    <span className="title mb-4">TRADITIONAL GAMES</span>
                                    <span className="description text-white w-75">
                    If you miss your childhood, we provide many traditional
                    games here
                                    </span>
                                </div>
                            </div>
                            <div className="d-flex mb-5">
                                <div className="me-5">
                                    <img src="images/index/dot-transparent.svg" alt="" />
                                </div>
                                <div className="d-flex flex-column">
                                    <span className="title">GAME SUIT</span>
                                </div>
                            </div>
                            <div className="d-flex mb-5">
                                <div className="me-5">
                                    <img src="images/index/dot-transparent.svg" alt="" />
                                </div>
                                <div className="d-flex mb-5">
                                    <span className="title">TBD</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="container-fluid d-flex system-background">
                    <div className="container-fluid background-radius"></div>
                    <div className="d-flex flex-column align-items-center justify-content-center system-content w-100">
                        <div className="system-title text-white">
              Can My Computer Run this game?
                        </div>
                        <div className="d-flex flex-column system-item">
                            <div className="item-title text-white mb-4">
                SYSTEM
                                <br />
                REQUIREMENTS
                            </div>
                            <div className="item-table text-white">
                                <table className="table table-bordered">
                                    <tr>
                                        <td>
                                            <span className="table-title">
                        OS:
                                                <br />
                                            </span>
                                            <span className="table-description">
                        Windows 7 64-bit only (No OSX support at this time)
                                            </span>
                                        </td>
                                        <td>
                                            <span className="table-title">
                        PROCESSOR:
                                                <br />
                                            </span>
                                            <span className="table-description">
                        Intel Core 2 Duo @ 2.4 GHZ or AMD Athlon X2 @ 2.8 GHZ
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span className="table-title">
                        MEMORY:
                                                <br />
                                            </span>
                                            <span className="table-description">4 GB RAM</span>
                                        </td>
                                        <td>
                                            <span className="table-title">
                        STORAGE:
                                                <br />
                                            </span>
                                            <span className="table-description">
                        8 GB available space
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colSpan="2">
                                            <span className="table-title">
                        GRAPHICS:
                                                <br />
                                            </span>
                                            <span className="table-description">
                        NVIDIA GeForce GTX 660 2GB or AMD Radeon HD 7850 2GB
                        DirectX11 (Shader Model 5)
                                            </span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="container-fluid d-flex press-quote">
                    <img
                        src="images/index/top-score-bg.png"
                        alt=""
                        srcSet=""
                        className="bg-press-quote"
                    />
                    <div className="d-flex justify-content-center w-100 press-quote-content">
                        <div className="pq-left d-flex flex-column justify-content-center">
                            <span className="title text-white mb-3">TOP SCORES</span>
                            <span className="description text-white mb-5">
                This top score from various games provided on this platform
                            </span>
                            <button>see more</button>
                        </div>
                        <div className="pq-right justify-content-between">
                            <div className="w-100 h-100 d-flex flex-column justify-content-evenly">
                                <div className="d-flex top-score-1">
                                    <div className="box-card mb-4">
                                        <div className="d-flex align-items-center justify-content-between p-4">
                                            <div className="d-flex align-items-center identity">
                                                <div
                                                    className="position-relative me-4 identity-photo"
                                                    style="width: 60px; height: 60px;"
                                                >
                                                    <span className="background-photo">&nbsp;</span>
                                                    <img
                                                        src="images/index/photo-user-1.jpg"
                                                        alt=""
                                                        className="photo-user"
                                                    />
                                                </div>
                                                <div className="d-flex flex-column">
                                                    <span className="text-white pq-name">Evan Lahti</span>
                                                    <span className="text-white pq-job">PC Gamer</span>
                                                </div>
                                            </div>
                                            <div>
                                                <a href="#">
                                                    <img src="images/index/twitter.svg" alt="" />
                                                </a>
                                            </div>
                                        </div>
                                        <div className="d-flex flex-column px-4 pb-4">
                                            <span className="pq-comment text-white">
                        “One of my gaming highlights of the year.”
                                            </span>
                                            <span className="pq-date mt-3">June 18, 2021</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="d-flex top-score-2">
                                    <div className="box-card mb-4">
                                        <div className="d-flex align-items-center justify-content-between p-4">
                                            <div className="d-flex align-items-center identity">
                                                <div
                                                    className="position-relative me-4 identity-photo"
                                                    style="width: 60px; height: 60px;"
                                                >
                                                    <span className="background-photo">&nbsp;</span>
                                                    <img
                                                        src="images/index/photo-user-2.jpg"
                                                        alt=""
                                                        className="photo-user"
                                                    />
                                                </div>
                                                <div className="d-flex flex-column">
                                                    <span className="text-white pq-name">Jada Griffin</span>
                                                    <span className="text-white pq-job">Nerdreactor</span>
                                                </div>
                                            </div>
                                            <div>
                                                <a href="#">
                                                    <img src="images/index/twitter.svg" alt="" />
                                                </a>
                                            </div>
                                        </div>
                                        <div className="d-flex flex-column px-4 pb-4">
                                            <span className="pq-comment text-white">
                        “The next big thing in the world of streaming and
                        survival games.”
                                            </span>
                                            <span className="pq-date mt-3">July 10, 2021</span>
                                        </div>
                                    </div>
                                </div>
                                <div className="d-flex top-score-3">
                                    <div className="box-card">
                                        <div className="d-flex align-items-center justify-content-between p-4">
                                            <div className="d-flex align-items-center identity">
                                                <div
                                                    className="position-relative me-4 identity-photo"
                                                    style="width: 60px; height: 60px;"
                                                >
                                                    <span className="background-photo">&nbsp;</span>
                                                    <img
                                                        src="images/index/photo-user-3.jpg"
                                                        alt=""
                                                        className="photo-user"
                                                    />
                                                </div>
                                                <div className="d-flex flex-column">
                                                    <span className="text-white pq-name">Aaron Williams</span>
                                                    <span className="text-white pq-job">Uproxx</span>
                                                </div>
                                            </div>
                                            <div>
                                                <a href="#">
                                                    <img src="images/index/twitter.svg" alt="" />
                                                </a>
                                            </div>
                                        </div>
                                        <div className="d-flex flex-column px-4 pb-4">
                                            <span className="pq-comment text-white">
                        “Snoop Dogg Playing The Wildly Entertaining SOS Is
                        Ridiculous.”
                                            </span>
                                            <span className="pq-date mt-3">December 24, 2018</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="container-fluid d-flex justify-content-center newsletter mb-5">
                    <div className="container d-flex justify-content-center position-relative ">
                        <div className="d-flex justify-content-center align-items-center newsletter-content">
                            <div className="newsletter-left d-flex justify-content-center position-relative">
                                <span>&nbsp;</span>
                                <img src="images/index/image-newsletter.png" alt=""/>
                            </div>
                            <div className="newsletter-right d-flex flex-column">
                                <div className="d-flex flex-column mb-5">
                                    <span className="text-white subtitle mb-4">Want to stay in touch?</span>
                                    <span className="text-white title mb-4">NEWSLETTER SUBSCRIBE </span>
                                    <span className="text-white description">In order to start receiving our news, all you have to do is enter your email address. Everything else will be taken care of by us. We will send you emails containing information about game. We dont spam.</span>
                                </div>
                                <div className="d-flex form-subscribe">
                                    <div className="form-floating">
                                        <input type="email" className="form-control newsletter-input" id="floatingInput" placeholder="youremail@binar.co.id"/>
                                        <label htmlFor="floatingInput" className="newsletter-label">Your email address</label>
                                    </div>
                                    <div className="form-button">
                                        <button className="newsletter-button">Subscribe now</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <footer className="position-absolute bottom-0 w-100">
                            <div className="d-flex mb-4 footer-top">
                                <div className="d-flex footer-top-1">
                                    <div className="">
                                        <a href="#">MAIN</a>
                                    </div>
                                    <div className="">
                                        <a href="#">ABOUT</a>
                                    </div>
                                    <div className="">
                                        <a href="#">GAME FEATURES</a>
                                    </div>
                                    <div className="">
                                        <a href="#">SYSTEM REQUIREMENTS</a>
                                    </div>
                                    <div className="">
                                        <a href="#">QUOTES</a>
                                    </div>
                                </div>
                                <div className="d-flex footer-top-2">
                                    <div className="icon-facebook">
                                        <a href="#">
                                            <img src="images/index/facebook.svg" alt=""/>
                                        </a>
                                    </div>
                                    <div className="icon-twitter">
                                        <a href="#">
                                            <img src="images/index/twitter.svg" alt=""/>
                                        </a>
                                    </div>
                                    <div className="icon-youtube">
                                        <a href="#">
                                            <img src="images/index/Vector.svg" alt=""/>
                                        </a>
                                    </div>
                                    <div className="icon-twitch">
                                        <a href="#">
                                            <img src="images/index/twitch.svg" alt=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <hr/>
                            <div className="d-flex justify-content-between mt-4 footer-bottom">
                                <div className="bottom">© 2018 Your Games, Inc. All Rights Reserved</div>
                                <div className="policy">
                                    <a href="#" className="me-2">PRIVACY POLICY</a>
                                    <span className="bottom">|</span>
                                    <a href="#" className="mx-2">TERMS OF SERVICE</a>
                                    <span className="bottom">|</span>
                                    <a href="#" className="ms-2">CODE OF CONDUCT</a>
                                </div>
                            </div>
                        </footer>
                    </div>
                </section>
                <script src="js/index/script.js"></script>
            </body>
        );
    }
}
export default Home;
