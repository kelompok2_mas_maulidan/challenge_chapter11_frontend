import React, { useState } from 'react'
import {Player, ControlBar} from 'video-react'
import 'video-react/dist/video-react.css';

const sources = {
  sintelTrailer: 'http://media.w3.org/2010/05/sintel/trailer.mp4',
  bunnyTrailer: 'http://media.w3.org/2010/05/bunny/trailer.mp4',
  bunnyMovie: 'http://media.w3.org/2010/05/bunny/movie.mp4',
  test: 'http://media.w3.org/2010/05/video/movie_300.webm',
} 

function App() {
  const [player, setPlayer] = useState()
  const [source, setSource] = useState(sources.bunnyMovie)
  const play=()=>{
    player.play()
  }
  const pause=()=>{
    player.pause()
  }
  const load=()=>{
    player.load()
  }

  return (
    <div>
      <Player>
        <source src={'https://res.cloudinary.com/dam5dp3mq/video/upload/v1664261544/samples/cld-sample-video.mp4'}/>
        <ControlBar autoHide={false}/>
      </Player>
      <a>aasas</a>
    </div>
    
  );
}
export default App;