import { cleanup, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import App from './Login';

afterEach(() => {
  cleanup();
})

describe("Login Component", () => {
  test('renders Sign Up link', () => {
    render(<App />);
    const signUpElement = screen.getByText("Sign Up");
    expect(signUpElement).toBeInTheDocument();
  });

  test('renders Back link', () => {
    render(<App />);
    const backElement = screen.getByText("Back");
    expect(backElement).toBeInTheDocument();
  });
});

