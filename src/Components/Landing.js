import React, { Component } from 'react';

class Landing extends Component {
    
    render() {
        return (
            <div>
                {/* <nav class="navbar navbar-expand-lg navbar-light w-100">
          <div class="container position-relative">
            <a class="navbar-brand text-white" href="#">
              LOGO
            </a>
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i
                class="fa fa-bars"
                aria-hidden="true"
                style="display: block;"
              ></i>
              <i
                class="fa fa-times"
                aria-hidden="true"
                style="display: none;"
              ></i>
            </button>
            <div
              class="collapse navbar-collapse justify-content-between"
              id="navbarSupportedContent"
            >
              <div>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                    <a class="nav-link text-white" href="#">
                      HOME
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="#">
                      WORK
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="#">
                      CONTACT
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="#">
                      ABOUT ME
                    </a>
                  </li>
                </ul>
              </div>
              <div>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                  <li class="nav-item">
                    <a class="nav-link text-white" href="#">
                      SIGN UP
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link text-white" href="#">
                      LOGIN
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </nav> */}

                <section className="container-fluid main-background">
                    <div className="home-content d-flex flex-column justify-content-center align-items-center position-relative">
                        <div className="text-white home-title mb-5">
                            <span>PLAY TRADITIONAL GAME</span>
                        </div>
                        <div className="text-white home-description mb-5">
                            <span>Experience new traditional game play</span>
                        </div>
                        <div className="home-button">
                            <button onClick="location.href = '/game';">PLAY NOW</button>
                        </div>
                        <div className="home-navigator position-absolute">
                            <a href="#" className="d-flex flex-column align-items-center">
                                <span>THE STORY</span>
                                <img src="../public/images/index/scroll-down.svg" alt="" />
                            </a>
                        </div>
                    </div>
                </section>

                {/* <section class="container-fluid d-flex the-games-background">
          <div class="the-games-content d-flex align-items-center justify-content-center">
            <div class="the-games-text d-flex flex-column">
              <span class="the-games-text-top text-white">
                What's so special?
              </span>
              <span class="the-games-text-bottom text-white">THE GAMES</span>
            </div>
            <div class="the-games-carousel">
              <div
                id="carouselExampleIndicators"
                class="carousel slide"
                data-bs-ride="carousel"
              >
                <div class="carousel-indicators">
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="0"
                    class="active"
                    aria-current="true"
                    aria-label="Slide 1"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="1"
                    aria-label="Slide 2"
                  ></button>
                  <button
                    type="button"
                    data-bs-target="#carouselExampleIndicators"
                    data-bs-slide-to="2"
                    aria-label="Slide 3"
                  ></button>
                </div>
                <div class="carousel-inner">
                  <div class="carousel-item active">
                    <img
                      src="images/index/rockpaperstrategy-1600.jpg"
                      class="d-block w-100"
                      alt="rockpaperstrategy-1600"
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      src="images/index/rockpaperstrategy-1600.jpg"
                      class="d-block w-100"
                      alt="rockpaperstrategy-1600"
                    />
                  </div>
                  <div class="carousel-item">
                    <img
                      src="images/index/rockpaperstrategy-1600.jpg"
                      class="d-block w-100"
                      alt="rockpaperstrategy-1600"
                    />
                  </div>
                </div>
                <button
                  class="carousel-control-prev"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="prev"
                >
                  <span
                    class="carousel-control-prev-icon"
                    aria-hidden="true"
                  ></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button
                  class="carousel-control-next"
                  type="button"
                  data-bs-target="#carouselExampleIndicators"
                  data-bs-slide="next"
                >
                  <span
                    class="carousel-control-next-icon"
                    aria-hidden="true"
                  ></span>
                  <span class="visually-hidden">Next</span>
                </button>
              </div>
            </div>
          </div>
        </section>

        <section class="container-fluid d-flex justify-content-end align-items-center features-background">
          <div class="d-flex flex-column mobile-card-glass">
            <div class="d-flex flex-column mb-5">
              <span class="features-subtitle text-white">
                What's so special
              </span>
              <span class="features-title text-white">FEATURES</span>
            </div>
            <div class="d-flex flex-column features-timeline">
              <div class="d-flex mb-5">
                <div class="me-5">
                  <img src="images/index/dot-white.svg" alt="" />
                </div>
                <div class="d-flex flex-column">
                  <span class="title mb-4">TRADITIONAL GAMES</span>
                  <span class="description text-white w-75">
                    If you miss your childhood, we provide many traditional
                    games here
                  </span>
                </div>
              </div>
              <div class="d-flex mb-5">
                <div class="me-5">
                  <img src="images/index/dot-transparent.svg" alt="" />
                </div>
                <div class="d-flex flex-column">
                  <span class="title">GAME SUIT</span>
                </div>
              </div>
              <div class="d-flex mb-5">
                <div class="me-5">
                  <img src="images/index/dot-transparent.svg" alt="" />
                </div>
                <div class="d-flex mb-5">
                  <span class="title">TBD</span>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="container-fluid d-flex system-background">
          <div class="container-fluid background-radius"></div>
          <div class="d-flex flex-column align-items-center justify-content-center system-content w-100">
            <div class="system-title text-white">
              Can My Computer Run this game?
            </div>
            <div class="d-flex flex-column system-item">
              <div class="item-title text-white mb-4">
                SYSTEM
                <br />
                REQUIREMENTS
              </div>
              <div class="item-table text-white">
                <table class="table table-bordered">
                  <tr>
                    <td>
                      <span class="table-title">
                        OS:
                        <br />
                      </span>
                      <span class="table-description">
                        Windows 7 64-bit only (No OSX support at this time)
                      </span>
                    </td>
                    <td>
                      <span class="table-title">
                        PROCESSOR:
                        <br />
                      </span>
                      <span class="table-description">
                        Intel Core 2 Duo @ 2.4 GHZ or AMD Athlon X2 @ 2.8 GHZ
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <span class="table-title">
                        MEMORY:
                        <br />
                      </span>
                      <span class="table-description">4 GB RAM</span>
                    </td>
                    <td>
                      <span class="table-title">
                        STORAGE:
                        <br />
                      </span>
                      <span class="table-description">
                        8 GB available space
                      </span>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2">
                      <span class="table-title">
                        GRAPHICS:
                        <br />
                      </span>
                      <span class="table-description">
                        NVIDIA GeForce GTX 660 2GB or AMD Radeon HD 7850 2GB
                        DirectX11 (Shader Model 5)
                      </span>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </div>
        </section>

        <section class="container-fluid d-flex press-quote">
          <img
            src="images/index/top-score-bg.png"
            alt=""
            srcset=""
            class="bg-press-quote"
          />
          <div class="d-flex justify-content-center w-100 press-quote-content">
            <div class="pq-left d-flex flex-column justify-content-center">
              <span class="title text-white mb-3">TOP SCORES</span>
              <span class="description text-white mb-5">
                This top score from various games provided on this platform
              </span>
              <button>see more</button>
            </div>
            <div class="pq-right justify-content-between">
              <div class="w-100 h-100 d-flex flex-column justify-content-evenly">
                <div class="d-flex top-score-1">
                  <div class="box-card mb-4">
                    <div class="d-flex align-items-center justify-content-between p-4">
                      <div class="d-flex align-items-center identity">
                        <div
                          class="position-relative me-4 identity-photo"
                          style="width: 60px; height: 60px;"
                        >
                          <span class="background-photo">&nbsp;</span>
                          <img
                            src="images/index/photo-user-1.jpg"
                            alt=""
                            class="photo-user"
                          />
                        </div>
                        <div class="d-flex flex-column">
                          <span class="text-white pq-name">Evan Lahti</span>
                          <span class="text-white pq-job">PC Gamer</span>
                        </div>
                      </div>
                      <div>
                        <a href="#">
                          <img src="images/index/twitter.svg" alt="" />
                        </a>
                      </div>
                    </div>
                    <div class="d-flex flex-column px-4 pb-4">
                      <span class="pq-comment text-white">
                        “One of my gaming highlights of the year.”
                      </span>
                      <span class="pq-date mt-3">June 18, 2021</span>
                    </div>
                  </div>
                </div>
                <div class="d-flex top-score-2">
                  <div class="box-card mb-4">
                    <div class="d-flex align-items-center justify-content-between p-4">
                      <div class="d-flex align-items-center identity">
                        <div
                          class="position-relative me-4 identity-photo"
                          style="width: 60px; height: 60px;"
                        >
                          <span class="background-photo">&nbsp;</span>
                          <img
                            src="images/index/photo-user-2.jpg"
                            alt=""
                            class="photo-user"
                          />
                        </div>
                        <div class="d-flex flex-column">
                          <span class="text-white pq-name">Jada Griffin</span>
                          <span class="text-white pq-job">Nerdreactor</span>
                        </div>
                      </div>
                      <div>
                        <a href="#">
                          <img src="images/index/twitter.svg" alt="" />
                        </a>
                      </div>
                    </div>
                    <div class="d-flex flex-column px-4 pb-4">
                      <span class="pq-comment text-white">
                        “The next big thing in the world of streaming and
                        survival games.”
                      </span>
                      <span class="pq-date mt-3">July 10, 2021</span>
                    </div>
                  </div>
                </div>
                <div class="d-flex top-score-3">
                  <div class="box-card">
                    <div class="d-flex align-items-center justify-content-between p-4">
                      <div class="d-flex align-items-center identity">
                        <div
                          class="position-relative me-4 identity-photo"
                          style="width: 60px; height: 60px;"
                        >
                          <span class="background-photo">&nbsp;</span>
                          <img
                            src="images/index/photo-user-3.jpg"
                            alt=""
                            class="photo-user"
                          />
                        </div>
                        <div class="d-flex flex-column">
                          <span class="text-white pq-name">Aaron Williams</span>
                          <span class="text-white pq-job">Uproxx</span>
                        </div>
                      </div>
                      <div>
                        <a href="#">
                          <img src="images/index/twitter.svg" alt="" />
                        </a>
                      </div>
                    </div>
                    <div class="d-flex flex-column px-4 pb-4">
                      <span class="pq-comment text-white">
                        “Snoop Dogg Playing The Wildly Entertaining SOS Is
                        Ridiculous.”
                      </span>
                      <span class="pq-date mt-3">December 24, 2018</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>

        <section class="container-fluid d-flex justify-content-center newsletter mb-5">
        <div class="container d-flex justify-content-center position-relative ">
            <div class="d-flex justify-content-center align-items-center newsletter-content">
                <div class="newsletter-left d-flex justify-content-center position-relative">
                    <span>&nbsp;</span>
                    <img src="images/index/image-newsletter.png" alt=""/>
                </div>
                <div class="newsletter-right d-flex flex-column">
                    <div class="d-flex flex-column mb-5">
                        <span class="text-white subtitle mb-4">Want to stay in touch?</span>
                        <span class="text-white title mb-4">NEWSLETTER SUBSCRIBE </span>
                        <span class="text-white description">In order to start receiving our news, all you have to do is enter your email address. Everything else will be taken care of by us. We will send you emails containing information about game. We dont spam.</span>
                    </div>
                    <div class="d-flex form-subscribe">
                        <div class="form-floating">
                            <input type="email" class="form-control newsletter-input" id="floatingInput" placeholder="youremail@binar.co.id"/>
                            <label for="floatingInput" class="newsletter-label">Your email address</label>
                        </div>
                        <div class="form-button">
                            <button class="newsletter-button">Subscribe now</button>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="position-absolute bottom-0 w-100">
                <div class="d-flex mb-4 footer-top">
                    <div class="d-flex footer-top-1">
                        <div class="">
                            <a href="#">MAIN</a>
                        </div>
                        <div class="">
                            <a href="#">ABOUT</a>
                        </div>
                        <div class="">
                            <a href="#">GAME FEATURES</a>
                        </div>
                        <div class="">
                            <a href="#">SYSTEM REQUIREMENTS</a>
                        </div>
                        <div class="">
                            <a href="#">QUOTES</a>
                        </div>
                    </div>
                    <div class="d-flex footer-top-2">
                        <div class="icon-facebook">
                            <a href="#">
                                <img src="images/index/facebook.svg" alt=""/>
                            </a>
                        </div>
                        <div class="icon-twitter">
                            <a href="#">
                                <img src="images/index/twitter.svg" alt=""/>
                            </a>
                        </div>
                        <div class="icon-youtube">
                            <a href="#">
                                <img src="images/index/Vector.svg" alt=""/>
                            </a>
                        </div>
                        <div class="icon-twitch">
                            <a href="#">
                                <img src="images/index/twitch.svg" alt=""/>
                            </a>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="d-flex justify-content-between mt-4 footer-bottom">
                    <div class="bottom">© 2018 Your Games, Inc. All Rights Reserved</div>
                    <div class="policy">
                        <a href="#" class="me-2">PRIVACY POLICY</a>
                        <span class="bottom">|</span>
                        <a href="#" class="mx-2">TERMS OF SERVICE</a>
                        <span class="bottom">|</span>
                        <a href="#" class="ms-2">CODE OF CONDUCT</a>
                    </div>
                </div>
            </footer>
        </div>
    </section> */}
                <script src="js/index/script.js"></script>
            </div>
        );
    }
}
export default Landing;
