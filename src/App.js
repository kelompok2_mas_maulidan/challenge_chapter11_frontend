import logo from './logo.svg';
import './App.css';
import Router from './routes/router'
import { ToastContainer } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

function App() {
  return (
    <div className="App">
     <Router/>
     <ToastContainer />
    </div>
  );
}

export default App;
