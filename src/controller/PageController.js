import axios from 'axios';
import React,{useState,useEffect} from 'react';
import NavBar from '../Components/Navbar/Navbar';
import LandingPage from '../Components/LandingPage/LandingPage';
function HomePage(){
    return(<>
        <NavBar/>
        <LandingPage />
    </>
    );
}

export {HomePage};